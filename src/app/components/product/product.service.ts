import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//component
import { MatSnackBar } from '@angular/material/snack-bar'

import { Observable } from 'rxjs';
//modelo
import { Product } from './product.model';

@Injectable({
	//modulo Singleton
  providedIn: 'root'
})
export class ProductService {
	baseUrl = "http://localhost:3001/products"

	constructor(
		private snackBar: MatSnackBar, 
		private http: HttpClient) 
		{ }
	
	showMessage(msg: string): void {
		this.snackBar.open(msg, 'X', {
			duration: 3000,
			horizontalPosition: "right",
			verticalPosition: "top",
			panelClass: "primary"
		})
	}

	create(product: Product): Observable<Product>{
		return this.http.post<Product>(this.baseUrl, product)
	}
	
	get(): Observable<Product[]>{
		return this.http.get<Product[]>(this.baseUrl)
	}

}
