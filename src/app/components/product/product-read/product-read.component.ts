import { Product } from './../product.model';
import { ProductService } from './../product.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-read',
  templateUrl: './product-read.component.html',
  styleUrls: ['./product-read.component.css']
})
export class ProductReadComponent implements OnInit {

	products: Product[] = []

  constructor(private productService: ProductService) { }

  ngOnInit(): void { 
		this.getProducts()
	}
	
	getProducts(): void {
		this.productService.get()
			.subscribe(products => {
				this.products = products
			})
	}

}
