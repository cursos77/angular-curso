import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from './../product.service';
import { Product } from './../product.model';
import { isNull } from '@angular/compiler/src/output/output_ast';
import { isEmptyExpression } from '@angular/compiler';

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css']
})
export class ProductCreateComponent implements OnInit {

	product: Product = {
		name: "",
		price: 0
	}

  constructor(
		private productService: ProductService,
		private router: Router
	) { }

	ngOnInit(): void { }

	//* Event Bidings
	createProduct(): void {
		this.productService.create(this.product).
			subscribe(() => {
				this.productService.showMessage("Produto criado com sucesso!")
				this.clearProducts()
				// this.navigateProducts()
			}
		)
	}

	clearProducts(){
		this.product.name = ""
		this.product.price = 0
	}

	navigateProducts(): void {
		this.router.navigate(['/products'])
	}
	
	

}
